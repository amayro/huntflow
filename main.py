import json
import os
import sys
from argparse import ArgumentParser
from typing import List

import openpyxl
import requests

import settings
from decorators import logged
from logger import get_logger


class HuntflowScript:
    def __init__(self, debug, logs_show, api_token, email, db_dir, db_name):
        self.logger = get_logger(type(self).__name__, is_debug=debug, show_logs=logs_show)

        self.email = email
        self.api_token = api_token
        self.api_url = settings.api_url
        self.filename_state = settings.filename_state
        self.base_headers = {
            'User-Agent': f'App/1.0 ({self.email})',
            'Authorization': f'Bearer {self.api_token}',
        }

        self.db_dir = db_dir
        self.db_name = db_name
        self.db_path = os.path.join(self.db_dir, self.db_name)
        self.db_rows = {
            'position': 1,
            'name': 2,
            'money': 3,
            'comment': 4,
            'status': 5,
        }

        self._check_init_data()

    def _check_init_data(self):
        """Проверка на наличие токена и файла бд"""

        if not (self.api_token and os.path.exists(self.db_path)):
            self.close("Необходимо задать api_token, путь к базе данных и наименование базы данных")

    @staticmethod
    def dump_json(filename, data):
        """Записывает данные в json файл"""

        with open(filename, "w", encoding=settings.ENCODING_NAME) as file:
            file.write(json.dumps(data, ensure_ascii=False))

    @staticmethod
    def load_json(filename):
        """Считывает данные из json файла"""

        with open(filename, 'r', encoding=settings.ENCODING_NAME) as file:
            data = json.load(file)
        return data

    @logged
    def make_request(self, endpoint: str, method='GET', headers=None, **kwargs) -> dict:
        """Делает запрос к api Huntflow и возвращает ответ"""

        if headers is None:
            headers = {}
        headers.update(self.base_headers)
        r = requests.request(method=method, url=f'{self.api_url}{endpoint}', headers=headers, **kwargs)
        if r.status_code != 200:
            self.close(f'status_code={r.status_code} response={r.json()}')
        return r.json()

    def get_account_id(self) -> int:
        """Получение account_id"""

        accounts = self.get_accounts_info()
        return accounts['items'][0]['id']

    def get_status_id(self, account_id: int, status_name) -> int:
        """Получение status_id"""

        statuses = self.get_statuses_info(account_id)
        for status in statuses:
            if self.get_normalize_str(status_name) in self.get_normalize_str(status['name']):
                return status['id']

    def get_vacancy_id(self, account_id: int, position_name: str) -> int:
        """Получение vacancy_id"""

        vacancies = self.get_vacancies_info(account_id)
        for vacancy in vacancies:
            if self.get_normalize_str(position_name) in self.get_normalize_str(vacancy['position']):
                return vacancy['id']

    def get_accounts_info(self) -> dict:
        """Получение данных об аккаунтах"""

        return self.make_request('/accounts')

    def get_applicants_info(self, account_id: int) -> list:
        """Получение списка кандидатов"""

        return self.make_request(f'/account/{account_id}/applicants')['items']

    def get_statuses_info(self, account_id: int) -> list:
        """Получение списка этапов подбора компании"""

        return self.make_request(f'/account/{account_id}/vacancy/statuses')['items']

    def get_sources_info(self, account_id: int) -> dict:
        """Получение списка источников резюме компании"""

        return self.make_request(f'/account/{account_id}/applicant/sources')

    def get_vacancies_info(self, account_id: int) -> list:
        """Получение списка источников вакансий компании"""

        return self.make_request(f'/account/{account_id}/vacancies')['items']

    def get_candidate_info(self, account_id: int, filename: str) -> dict:
        """Загрузка и распознавание файлов"""

        return self.make_request(
            endpoint=f'/account/{account_id}/upload',
            method='POST',
            headers={'X-File-Parse': 'true'},
            files={'file': (os.path.basename(filename), open(filename, 'rb'), 'application/pdf')},
        )

    def get_candidate_db_info(self, row: int) -> dict:
        """Получение данных о кандидате из базы (excel)"""

        wb = openpyxl.load_workbook(self.db_path)
        ws = wb.active
        data = {}
        for name, value in self.db_rows.items():
            cell_value = ws.cell(row=row, column=value).value
            if cell_value:
                data[name] = str(cell_value).strip()

        return data

    def get_filename_candidate(self, name: str, position: str) -> str:
        """Получение имени файла с резюме кандидата по имени кандидата и его должности"""

        for path, dirs, files in os.walk(os.path.join(self.db_dir, position)):
            for file in files:
                if self.get_normalize_str(name) in self.get_normalize_str(file):
                    return os.path.join(path, file)

    def add_candidate_to_huntflow(self,
                                  account_id: int,
                                  candidate_raw: dict,
                                  candidate: dict,) -> dict:
        """
        Добавление кандидата на вакансию

        **Args**:

        ``candidate_raw``: информация о кандидате, полученная из файла бд (excel)

        ``candidate``: информация о кандидате, полученная в результате распознования файла с резюме
        """

        fields = candidate['fields']
        position_now = fields['experience'][0]['position'] if fields['experience'] else None
        company_now = fields['experience'][0]['company'] if fields['experience'] else None

        birthdate = fields['birthdate']
        birthday_day = birthdate['day'] if birthdate else None
        birthday_month = birthdate['month'] if birthdate else None
        birthday_year = birthdate['year'] if birthdate else None

        photo_id = candidate['photo']['id'] if candidate['photo'] else None

        data = {
            "last_name": fields['name']['last'],
            "first_name": fields['name']['first'],
            "middle_name": fields['name']['middle'],
            "phone": ', '.join(fields['phones']),
            "email": fields['email'],
            "position": position_now,
            "company": company_now,
            "money": candidate_raw['money'],
            "birthday_day": birthday_day,
            "birthday_month": birthday_month,
            "birthday_year": birthday_year,
            "photo": photo_id,
            "externals": [
                {
                    "data": {
                        "body": candidate['text']
                    },
                    "auth_type": "NATIVE",
                    "files": [
                        {
                            "id": candidate['id']
                        }
                    ],
                    "account_source": None
                }
            ]
        }

        return self.make_request(
            endpoint=f'/account/{account_id}/applicants',
            method='POST',
            json=data,
        )

    def add_candidate_to_vacancy(self,
                                 account_id: int,
                                 applicant_id: int,
                                 vacancy_id: int,
                                 status_id: int,
                                 comment: str,
                                 files: List[dict],) -> dict:
        """
        Добавление кандидата на вакансию

        **Args**:

        ``applicant_id``: идентификатор кандидата

        ``vacancy_id``: идентификатор вакансии

        ``status_id``: идентификатор статуса подбора

        ``comment``: произвольный комментарий

        ``files``: массив прикрепленных файлов к записи в истории (не больше 10)
        """

        data = {
            "vacancy": vacancy_id,
            "status": status_id,
            "comment": comment,
            "files": files[:10],
            "rejection_reason": None
        }

        return self.make_request(
            endpoint=f'/account/{account_id}/applicants/{applicant_id}/vacancy',
            method='POST',
            json=data,
        )

    @staticmethod
    def get_normalize_str(string: str):
        """Получение строки в нормализованном состоянии"""

        string_split = string.lower().replace('й', 'и').replace('̆', '').split()
        return ' '.join(string_split)

    def get_state_last_row(self):
        """Получение последнего номера строки, с которой было совершено взаимодействие"""

        if not os.path.exists(self.filename_state):
            state_row = 2
            self.dump_json(self.filename_state, {'state_row': state_row})
        else:
            state_row = self.load_json(self.filename_state)['state_row']
        return state_row

    def save_state_row(self, state_row):
        """Сохранение последнего номера строки, с которой было совершено взаимодействие"""

        data = self.load_json(self.filename_state)
        data.update({'state_row': state_row})
        self.dump_json(self.filename_state, data)

    def start(self):
        """Запуск скрипта"""

        account_id = self.get_account_id()

        while True:
            state_row = self.get_state_last_row()
            candidate_raw = self.get_candidate_db_info(state_row)
            if not candidate_raw:
                self.close('Все заявки обработаны.')

            candidate_filename = self.get_filename_candidate(candidate_raw['name'], candidate_raw['position'])
            candidate_info = self.get_candidate_info(account_id, candidate_filename)
            candidate = self.add_candidate_to_huntflow(account_id, candidate_raw, candidate_info)
            self.logger.info(f'Добавлен в базу huntflow: {candidate_raw["name"]}')

            status_id = self.get_status_id(account_id, candidate_raw['status'])
            vacancy_id = self.get_vacancy_id(account_id, candidate_raw['position'])

            # при добавлении кандидата в базу в ответе нету полей externals (вместо него external) и files
            self.add_candidate_to_vacancy(account_id=account_id,
                                          applicant_id=candidate['id'],
                                          vacancy_id=vacancy_id,
                                          status_id=status_id,
                                          comment=candidate_raw['comment'],
                                          files=[{"id": candidate_info['id']}])
            state_row += 1
            self.save_state_row(state_row)
            self.logger.info(f'Добавлен на вакансию: {candidate_raw["name"]}')

    def close(self, message=None):
        """Завершение работы скрипта"""

        msg = 'Close script.'
        if message:
            msg = f'{msg} {message}'
            self.logger.error(msg)
        else:
            self.logger.info(msg)
        sys.exit()


def parse_cli_args():
    """Разбор аргументов, передаваемых интерфейсом командной строки"""

    parser = ArgumentParser()
    parser.add_argument('-d', '--debug',
                        help='Sets debug mode',
                        action="store_true")

    parser.add_argument('--logs-show',
                        help='Output logs to the console. Default is True',
                        metavar=False,
                        default=True,
                        type=lambda x: str(x).capitalize() == 'True')

    parser.add_argument('-t', '--api_token',
                        type=str,
                        help='Huntflow api_token')

    parser.add_argument('-e', '--email',
                        type=str,
                        help='Email for user-agent')

    parser.add_argument('-dbp', '--db_path',
                        type=str,
                        help='Directory with database')

    parser.add_argument('-dbn', '--db_name',
                        type=str,
                        help='Database name')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_cli_args()

    if os.path.exists(settings.filename_credentials):
        with open(settings.filename_credentials, 'r', encoding=settings.ENCODING_NAME) as file:
            data = json.load(file)
            api_token = data['api_token']
            email = data['email']
    else:
        api_token = None
        email = None

    api_token = args.api_token if args.api_token else api_token
    email = args.email if args.email else email
    db_path = args.db_path if args.db_path else 'db'
    db_name = args.db_name if args.db_name else 'Тестовая база.xlsx'

    HuntflowScript(
        debug=args.debug,
        logs_show=args.logs_show,
        api_token=api_token,
        email=email,
        db_dir=db_path,
        db_name=db_name,
    ).start()
