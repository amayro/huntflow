import inspect
import logging
from functools import wraps

logger = logging.getLogger('HuntflowScript')


def logged(func):
    """Декоратор для отображения имени функции и запроса при логгировании"""

    @wraps(func)
    def wrapper(*args, **kwargs):
        logger.info(f'{inspect.stack()[1][3]} -> {func.__name__} - args={args}, kwargs={kwargs}')
        response = func(*args, **kwargs)
        logger.info(f'response={response}')
        return response

    return wrapper
